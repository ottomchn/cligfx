
# based heavily on code from Nadia Alramli

import sys

COLORS = 'BLUE GREEN CYAN RED MAGENTA YELLOW WHITE BLACK'.split()

# terminal CONTROLS
CONTROLS =  {'REVERSE': 'rev', 'DIM': 'dim', 'RIGHT': 'cuf1', 'BOLD': 'bold', 'CLEAR_EOL': 'el', 'NORMAL': 'sgr0', 'UP': 'cuu1', 'BLINK': 'blink', 'DOWN': 'cud1', 'CLEAR': 'clear', 'SHOW_CURSOR': 'cnorm', 'CR': 'cr', 'HIDE_CURSOR': 'cinvis', 'CLEAR_EOS': 'ed', 'CLEAR_CR': 'el1', 'UNDERLINE': 'smul', 'LEFT': 'cub1'}

# term capabilities
VALUES = { 'COLUMNS':'cols', 'LINES':'lines', 'MAX_COLORS':'colors' }

MODULE = sys.modules[__name__]

def caps():
  f = open('caps', 'w')
  for c in COLORS:
    sfg = "fg.{} {}\n".format(c, lookup[c].replace("\x1b", "\\x1b"))
    sbg = "bg.{} {}\n".format(c, lookup["BG_" + c].replace("\x1b", "\\x1b"))
    f.write(sfg)
    f.write(sbg)
    print sfg,sbg
    
  for c in CONTROLS:
    sc = "tf.{} {}\n".format(c, lookup[c].replace("\x1b", "\\x1b"))
    f.write(sc)
    print sc
    
  f.close()  

def nocurses():
    "clear all when curses in not installed"
    for c in COLORS:
        setattr(MODULE, c, '')
        setattr(MODULE, 'BG_%s' % c, '')
    for c in CONTROLS:
        setattr(MODULE, c, '')
    for v in VALUES:
        setattr(MODULE, v, None)

def setup():
    "setup the terminal"
    curses.setupterm()
    
    bg = curses.tigetstr('setab') or curses.tigetstr('setb') or ''
    fg = curses.tigetstr('setaf') or curses.tigetstr('setf') or ''
    
    for c in COLORS:
        # color indexes
        ci = getattr(curses, 'COLOR_%s' % c)
        # set color escape sequence
        setattr(MODULE, c, curses.tparm(fg, ci))
        # set bg escape sequence
        setattr(MODULE, 'BG_%s' % c, curses.tparm(bg, ci))

    for c in CONTROLS:
        # set control escape sequence
        setattr(MODULE, c, curses.tigetstr(CONTROLS[c]) or '')

    for v in VALUES:
        # set up term values
        setattr(MODULE, v, curses.tigetnum(VALUES[v]))

lookup = MODULE.__dict__
def format(text, fg='', bg='', tf='', norm=True):
    "format text for display on terminal"
    if text:
        text = str(text)
    if tf:
        text = lookup[tf.upper()] + text
    if fg:
        text = lookup[fg.upper()] + text
    if bg:
        text = lookup['BG_%s' % bg.upper()] + text
    if norm:
        text = text + lookup['NORMAL']
    return text

def format_fn(pre='', fg='', bg='', tf='', norm=True, post=''):
    "return a function that formats text based on spec"
    return lambda t: format(pre+str(t)+post, fg=fg, bg=bg, tf=tf, norm=norm)

def bg(color):
    "change background"
    sys.stdout.write(lookup['BG_%s' % color.upper()])

def fg(color):
    "change foreground"
    sys.stdout.write(lookup[color.upper()])

def tf(tf):
    sys.stdout.write(lookup[tf.upper()])

def normal():
    "reset to normal"
    sys.stdout.write(lookup['NORMAL'])

blank = lambda i: ''

try:
    import curses
    setup()
except:
    nocurses()
    print('curses could not be imported.')


def _make_helpers():
  i = 0
  j = 0
  for i in range(len(COLORS)):
    for j in range(len(COLORS)):
      func = "{0}_on_{1} = format_fn(fg='{0}', bg='{1}')"
      print(func.format(COLORS[i].lower(), COLORS[j].lower()))

# helpers
blue_on_blue = format_fn(fg='blue', bg='blue')
blue_on_green = format_fn(fg='blue', bg='green')
blue_on_cyan = format_fn(fg='blue', bg='cyan')
blue_on_red = format_fn(fg='blue', bg='red')
blue_on_magenta = format_fn(fg='blue', bg='magenta')
blue_on_yellow = format_fn(fg='blue', bg='yellow')
blue_on_white = format_fn(fg='blue', bg='white')
blue_on_black = format_fn(fg='blue', bg='black')
green_on_blue = format_fn(fg='green', bg='blue')
green_on_green = format_fn(fg='green', bg='green')
green_on_cyan = format_fn(fg='green', bg='cyan')
green_on_red = format_fn(fg='green', bg='red')
green_on_magenta = format_fn(fg='green', bg='magenta')
green_on_yellow = format_fn(fg='green', bg='yellow')
green_on_white = format_fn(fg='green', bg='white')
green_on_black = format_fn(fg='green', bg='black')
cyan_on_blue = format_fn(fg='cyan', bg='blue')
cyan_on_green = format_fn(fg='cyan', bg='green')
cyan_on_cyan = format_fn(fg='cyan', bg='cyan')
cyan_on_red = format_fn(fg='cyan', bg='red')
cyan_on_magenta = format_fn(fg='cyan', bg='magenta')
cyan_on_yellow = format_fn(fg='cyan', bg='yellow')
cyan_on_white = format_fn(fg='cyan', bg='white')
cyan_on_black = format_fn(fg='cyan', bg='black')
red_on_blue = format_fn(fg='red', bg='blue')
red_on_green = format_fn(fg='red', bg='green')
red_on_cyan = format_fn(fg='red', bg='cyan')
red_on_red = format_fn(fg='red', bg='red')
red_on_magenta = format_fn(fg='red', bg='magenta')
red_on_yellow = format_fn(fg='red', bg='yellow')
red_on_white = format_fn(fg='red', bg='white')
red_on_black = format_fn(fg='red', bg='black')
magenta_on_blue = format_fn(fg='magenta', bg='blue')
magenta_on_green = format_fn(fg='magenta', bg='green')
magenta_on_cyan = format_fn(fg='magenta', bg='cyan')
magenta_on_red = format_fn(fg='magenta', bg='red')
magenta_on_magenta = format_fn(fg='magenta', bg='magenta')
magenta_on_yellow = format_fn(fg='magenta', bg='yellow')
magenta_on_white = format_fn(fg='magenta', bg='white')
magenta_on_black = format_fn(fg='magenta', bg='black')
yellow_on_blue = format_fn(fg='yellow', bg='blue')
yellow_on_green = format_fn(fg='yellow', bg='green')
yellow_on_cyan = format_fn(fg='yellow', bg='cyan')
yellow_on_red = format_fn(fg='yellow', bg='red')
yellow_on_magenta = format_fn(fg='yellow', bg='magenta')
yellow_on_yellow = format_fn(fg='yellow', bg='yellow')
yellow_on_white = format_fn(fg='yellow', bg='white')
yellow_on_black = format_fn(fg='yellow', bg='black')
white_on_blue = format_fn(fg='white', bg='blue')
white_on_green = format_fn(fg='white', bg='green')
white_on_cyan = format_fn(fg='white', bg='cyan')
white_on_red = format_fn(fg='white', bg='red')
white_on_magenta = format_fn(fg='white', bg='magenta')
white_on_yellow = format_fn(fg='white', bg='yellow')
white_on_white = format_fn(fg='white', bg='white')
white_on_black = format_fn(fg='white', bg='black')
black_on_blue = format_fn(fg='black', bg='blue')
black_on_green = format_fn(fg='black', bg='green')
black_on_cyan = format_fn(fg='black', bg='cyan')
black_on_red = format_fn(fg='black', bg='red')
black_on_magenta = format_fn(fg='black', bg='magenta')
black_on_yellow = format_fn(fg='black', bg='yellow')
black_on_white = format_fn(fg='black', bg='white')
black_on_black = format_fn(fg='black', bg='black')
