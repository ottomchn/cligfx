# -*- coding: utf-8 -*-

#once again thanks to Nadia Alramli

import fmt, sys

class ProgressBar(object):
    """progress bar class"""
    TEMPLATE = (
     '%(percent)s[%(block)s%(empty)s] %(message)s\n'
    )
    PADDING = 7
 
    def __init__(self, percent_format=None, progress_format=None, block_format=None, empty_format=None, width=None, block='█', empty=' '):
        """
        bar_format      -- use fmt.format_fn
        percent_format  -- use fmt.format_fn
        progress_format -- use fmt.format_fn
        empty_format    -- use fmt.format_fn
        width           -- bar width (optional)
        block           -- progress display character (default '█')
        empty           -- bar display character (default ' ')
        """
        if block_format:
            self.block_format = block_format
        else:
            self.block_format = fmt.format_fn()

        if percent_format:
            self.percent_format = percent_format
        else:
            self.percent_format = fmt.format_fn(post='% ')

        if empty_format:
            self.empty_format = empty_format
        else:
            self.empty_format = fmt.format_fn()

        if width and width < fmt.COLUMNS - self.PADDING:
            self.width = width
        else:
            # Adjust to the width of the terminal
            self.width = fmt.COLUMNS - self.PADDING
        self.block = block
        self.empty = empty
        self.progress = None
        self.lines = 0
 
    def render(self, percent, message = '', msg_format=None):
        """Print the progress bar
        percent    -- the progress percentage %
        message    -- message string (optional)
        msg_format -- how to format (optional)
        """

        inline_msg_len = 0
        if message:
            # The length of the first line in the message
            inline_msg_len = len(message.splitlines()[0])
        if inline_msg_len + self.width + self.PADDING > fmt.COLUMNS:
            # The message is too long to fit in one line.
            # Adjust the bar width to fit.
            bar_width = fmt.COLUMNS - inline_msg_len -self.PADDING
        else:
            bar_width = self.width
 
        if msg_format == None:
            msg_format = fmt.format_fn()

        # Check if render is called for the first time
        if self.progress != None:
            self.clear()

        self.progress = int( (bar_width * percent) / 100 )
        data = self.TEMPLATE % {
            'percent': self.percent_format(percent),
            'block': self.block_format(self.block * self.progress),
            'empty': self.empty_format(self.empty * (bar_width - self.progress)),
            'message': msg_format(message)
        }
                
        sys.stdout.write(data)
        sys.stdout.flush()
        # The number of lines printed
        self.lines = len(data.splitlines())
 
    def clear(self):
        """Clear all printed lines"""
        sys.stdout.write(
            self.lines * (fmt.UP + fmt.CR + fmt.CLEAR_EOL)
        )

import time

def test_ProgressBar(p):
    #p.percent_format = fmt.blank
    for i in range(101):
        p.render(i, 'step %s\nTesting Bar\nPlease Wait...' % i)
        time.sleep(0.1)


if __name__ == "__main__":
  p = ProgressBar()
  test_ProgressBar(p)
